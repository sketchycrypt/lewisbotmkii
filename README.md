# lewisbotmkii / Lewis Bot Mk. II






## Description

I'm re-writing my old discord bot for my friends server

## Status

Currently fixed, but a barebones version.

## License

This project is under the GPL-3 license, meaning it is open source.  
I don't know _why_ you would use this repository though.

## Credits

made by sketchycrypt for a friend server.

## To-do
- [x] Get the bot to function
- [x] Add the catch command 
    - [x] Style
    - [x] Functionality
        - [x] Check for customId in catch.js
        - [x] Timeout user for 1 minute after 50/50 chance
        - [x] Make bot join when Lewis joins a VC and blast audio from a selected folder
- [x] Rewrite all the commands

