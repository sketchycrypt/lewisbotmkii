const { SlashCommandBuilder, ActionRowBuilder, ButtonBuilder, ButtonStyle, EmbedBuilder, Events } = require('discord.js')
const { Client } = require('../index')

module.exports = {
	data: new SlashCommandBuilder()
		.setName('catch')
		.setDescription('catch the silly emily!'),

	async execute(interaction) {
		const row = new ActionRowBuilder()
			.addComponents(
				new ButtonBuilder()
					.setCustomId('catchYes')
					.setLabel('Yes!')
					.setStyle(ButtonStyle.Primary),
				new ButtonBuilder()
					.setCustomId('catchNo')
					.setLabel('No.')
					.setStyle(ButtonStyle.Secondary)
			);

		const embed = new EmbedBuilder()
			.setColor(0x0099FF)
			.setTitle('Catch')
			.setDescription('You have found a rare wild Emily! Do you want to catch her?');

		await interaction.reply({ components: [row], embeds: [embed] });

	},
};

