const { SlashCommandBuilder } = require('@discordjs/builders')

module.exports = {
	data: new SlashCommandBuilder()
		.setName('phenomenal')
		.setDescription('Simply phenomenal.'),
	async execute(interaction){
		await interaction.reply('https://media3.giphy.com/media/gsGLcR9UxAsebfQBd8/giphy.mp4?cid=73b8f7b17a7a38d0bcba25968bf34c6aea79b0b48c8da81e&rid=giphy.mp4&ct=g');
	},
};
