const { Client, GatewayIntentBits, Collection, Events } = require('discord.js');
const dotenv = require('dotenv');
const fs = require("node:fs");
const path = require("node:path");
const chalk = require('chalk');
const vDiscord = require('@discordjs/voice')
var files = fs.readdirSync('./audio');

dotenv.config();

const client = new Client({
	intents: [
		GatewayIntentBits.GuildVoiceStates,
		GatewayIntentBits.GuildMessages,
		GatewayIntentBits.Guilds,
	],
});

client.commands = new Collection();

const cmdsPath = path.join(__dirname, 'commands');
const cmdFiles = fs.readdirSync(cmdsPath).filter(file => file.endsWith('.js'));

for (const file of cmdFiles){
	const filePath = path.join(cmdsPath, file);
	const cmd = require(filePath);
	if('data' in cmd && 'execute' in cmd) {
		client.commands.set(cmd.data.name, cmd);
	}else {
		console.log(`[WARNING] the cmd at ${filePath} is missing a required "data" or "execute property"`);
	}
}

client.on('ready', () =>{
	console.log(chalk.blue('it has begun'))
})

client.on(Events.InteractionCreate, async interaction => {
	if (!interaction.isChatInputCommand()) return;
	const command = interaction.client.commands.get(interaction.commandName);

	if (!command) {
		console.error(`No command matching ${interaction.commandName} was found.`);
		return;
	}

	try {
		await command.execute(interaction);
	} catch (error) {
		console.error(error);
		await interaction.reply({ content: 'There was an error while executing this command!', ephemeral: true });
	}

})

function getRandomInt(max) {
	return Math.floor(Math.random() * max);
}

client.on(Events.InteractionCreate, interaction => {
	// if(interaction.customId = 'catchYes'){
	switch(interaction.customId){
		case 'catchYes':
			let chance = getRandomInt(10);
			if(chance < 2){
				console.log(chance)
				interaction.reply(`${interaction.user} has caught a **RARE** Emily!`)
			}else if(chance > 2){
				try{
					console.log(chance)
					interaction.reply(`The poor bastard, ${interaction.user} didnt catch Emily, they have been timed out for 60 seconds.`)
					// interaction.member.disableCommunicationUntil(Date.now() + (1 * 60 * 1000), 'Did not catch Emily.')
				}catch {
					console.log(error)
				}
			}
			break;
		case 'catchNo':
			interaction.reply(`You coward, ${interaction.user}`)
			break;
	}
});

client.setMaxListeners(2);
client.on("voiceStateUpdate", (oldState, newState) =>{
	if(
		newState.member.id == "218027442357534721" &&
		oldState.channelId === null
	) {
		var chosenFile = files[Math.floor(Math.random() * files.length)];
		console.log(chosenFile)
		var player = vDiscord.createAudioPlayer();
		var resource = vDiscord.createAudioResource("audio/" + chosenFile);
		var connection = vDiscord.joinVoiceChannel({
			channelId: newState.channelId,
			guildId: newState.guild.id,
			adapterCreator: newState.guild.voiceAdapterCreator,
		})
		player.play(resource)
		player.on("error", console.error)
		console.log(chalk.red("The beast has arrived."))
		connection.subscribe(player)
		player.on(vDiscord.AudioPlayerStatus.Idle, () => {
			connection.destroy();
			var chosenFile = files[Math.floor(Math.random() * files.length)]
			console.log(chalk.cyan("The new file is " + chosenFile))
		})
	}
})


client.login(process.env.TOKEN);
